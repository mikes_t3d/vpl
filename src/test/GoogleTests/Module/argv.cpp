//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Module/Argv.h>

namespace argv
{
//! Testing method parse and clear of parsed values
TEST(ArgvTest, NonSimpleValue)
{
	//! Input values for parsing
	char const *a[3] = { "test.exe","-a", "xxx" };

	//! Parse inputed values
	vpl::mod::CArgv argv;
	argv.parse(3, const_cast<char**>(a));

	//! Checks valid counts of parsed values
	ASSERT_EQ(0, argv.getNumOfSimpleValues());
	ASSERT_EQ(1, argv.getNumOfValues());
}


//! Testing parsing with simple value then clearing all.
TEST(ArgvTest, SimpleValueAndClear)
{
	//! Input values for parsing
    char const *a[4] = { "test.exe", "-b", "yyy","z" };

	vpl::mod::CArgv argv;
    argv.parse(4, const_cast<char**>(a));

	//! Checks valid counts of parsed values
    ASSERT_EQ(1, argv.getNumOfSimpleValues());
    ASSERT_EQ(1, argv.getNumOfValues());

    argv.clear();

    //! Checks after clear method
	ASSERT_EQ(0, argv.getNumOfSimpleValues());
    ASSERT_EQ(0, argv.getNumOfValues());
}

//! Test for method exist. First value not exist then yes.
TEST(ArgvTest, Exist)
{
	vpl::mod::CArgv argv;
    ASSERT_FALSE(argv.exists("a")) << "Argument exist.";

    char const *a[2] = { "test.exe","-a" };
    argv.parse(2, const_cast<char**>(a));

    ASSERT_TRUE(argv.exists("a")) << "Argument not exist.";

}


}