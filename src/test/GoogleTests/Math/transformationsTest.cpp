//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#if defined _M_X64

#include "gtest/gtest.h"
#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/Quaternion.h>
#include <VPL/Test/Compare/compare1D.h>
#include <VPL/Test/Compare/compare2D.h>

namespace transformation
{

template <class T>
class TransformationTest : public testing::Test
{
public:
    typedef  T type;

    vpl::math::CStaticVector<type, 1> vector1;
    vpl::math::CStaticVector<type, 2> vector2;
    vpl::math::CStaticVector<type, 3> vector3;
    vpl::math::CStaticVector<type, 3> vector3A;
    vpl::math::CStaticVector<type, 3> vector3B;
    vpl::math::CQuat<type> quat;

    vpl::math::CTransformMatrix<type> transformMatrix;

    vpl::test::Compare1D<type, vpl::math::CStaticVector<type, 3>> compareStaticVector;
    vpl::test::Compare1D<type, vpl::math::CQuat<type>> compareQuat;
    vpl::test::Compare2D<type, vpl::math::CTransformMatrix<type>> compare2D;


    void SetUp() override
    {
        vector1 = vpl::math::CStaticVector<type, 1>(1);
        vector2 = vpl::math::CStaticVector<type, 2>(1, 2);
        vector3 = vpl::math::CStaticVector<type, 3>(1, 2, 3);

        vector3A = vpl::math::CStaticVector<type, 3>(1.0, 0.0, 0.0);
        vector3B = vpl::math::CStaticVector<type, 3>(0.0, 1.0, 0.0);

        quat = vpl::math::CQuat<type>(vpl::math::HALF_PI,
            vpl::math::CStaticVector<type, 3>(0.0, 0.0, 1.0));

        compareStaticVector.setErrorMessage("Vectors is differ at index");
        compareQuat.setErrorMessage("Quat is different at index");

        compare2D.setArrayOrder(vpl::test::ArrayOrder::COLUMN_MAJOR);
        compare2D.setErrorMessage("Matrix is different at index");
    }
};

//! Define types for testing
typedef testing::Types< double, float> implementations;
TYPED_TEST_CASE(TransformationTest, implementations);

TYPED_TEST(TransformationTest, Initialize)
{
    using type = typename TestFixture::type;

    EXPECT_EQ(1, TestFixture::vector1(0));
    EXPECT_EQ(1, TestFixture::vector2(0));

    EXPECT_EQ(1, TestFixture::vector3(0));
    EXPECT_EQ(2, TestFixture::vector3(1));
    EXPECT_EQ(3, TestFixture::vector3(2));

    EXPECT_EQ(1, TestFixture::vector1.size());
    EXPECT_EQ(2, TestFixture::vector2.size());
    EXPECT_EQ(3, TestFixture::vector3.size());

    type test[] = { 0.0,0.0,0.707107,0.707107 };
    TestFixture::compareQuat.values(test, TestFixture::quat, 3);


    type testTm[] = { 1,0,0,0, 0,1,0,0 , 0,0,1,0 , 0,0,0,1 };

    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);
}

TYPED_TEST(TransformationTest, Opossite)
{
    using type = typename TestFixture::type;

    type test[] = { -1,-2,-3 };
    TestFixture::compareStaticVector.values(test, -TestFixture::vector3, 3);
}

TYPED_TEST(TransformationTest, Set)
{
    using type = typename TestFixture::type;

    type test[] = { 1,2,3 };
    TestFixture::compareStaticVector.values(test, TestFixture::vector3, 3);
    TestFixture::vector3.set(1.0, 0.0, 0.0);

    type test2[] = { 1.0,0.0,0.0 };
    TestFixture::compareStaticVector.values(test2, TestFixture::vector3, 3);

    type testTm[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16 };
    TestFixture::transformMatrix.set(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);
}

TYPED_TEST(TransformationTest, ConstructFromStaticMatrix)
{
    using type = typename TestFixture::type;

    { // Construct from 3x3 matrix
        vpl::math::CStaticMatrix<type, 3, 3> m3;
        m3.ones();

        vpl::math::CTransformMatrix<type> tm{ m3 };
        vpl::math::CTransformMatrix<type> expected{
            1, 1, 1, 0,
            1, 1, 1, 0,
            1, 1, 1, 0,
            0, 0, 0, 1
        };
        EXPECT_EQ(tm, expected);
    }

    { // Construct from 4x4 matrix
        vpl::math::CStaticMatrix<type, 4, 4> m4;
        m4.ones();

        vpl::math::CTransformMatrix<type> tm{ m4 };
        EXPECT_EQ(m4, tm);
    }
}

TYPED_TEST(TransformationTest, Length)
{
    using type = typename TestFixture::type;

    if (vpl::CTypeTraits<type>::isIntegral)
    {
        return;
    }

    ASSERT_NEAR(3.742, TestFixture::vector3.length(), 0.01);
    ASSERT_NEAR(14.002, TestFixture::vector3.length2(), 0.01);
    TestFixture::vector3.normalize();
    ASSERT_NEAR(1.0, TestFixture::vector3.length(), 0.01);
    type test[] = { 0.267,0.534,0.802 };
    TestFixture::compareStaticVector.values(test, TestFixture::vector3, 0.01);
}

TYPED_TEST(TransformationTest, DotCross)
{
    using type = typename TestFixture::type;

    TestFixture::vector3.set(1.0, 0.0, 0.0);

    ASSERT_DOUBLE_EQ(0.0, TestFixture::vector3 * TestFixture::vector3B);

    vpl::math::CStaticVector<type, 3> v3(TestFixture::vector3 ^ TestFixture::vector3B);
    type test[] = { 0.0,0.0,1.0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}

TYPED_TEST(TransformationTest, RotationQuat)
{
    using type = typename TestFixture::type;

    vpl::math::CStaticVector<type, 3> v3 = TestFixture::quat * TestFixture::vector3A;
    type test[] = { -0,1,0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}

TYPED_TEST(TransformationTest, Inverse)
{
    using type = typename TestFixture::type;

    type testTm[] = { 1,0,0,0, 0,1,0,0 , 0,0,1,0 , 0,0,0,1 };
    vpl::math::CTransformMatrix<type>::inverse(TestFixture::transformMatrix);
    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);

    vpl::math::CStaticVector<type, 3> v3 = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 1,0,0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}

TYPED_TEST(TransformationTest, Translation)
{
    using type = typename TestFixture::type;

    TestFixture::transformMatrix.setTrans(vpl::math::CStaticVector<type, 3>(2.0, 1.0, 0.0));

    type testTm[] = { 1,0,0,2, 0,1,0,1 , 0,0,1,0 , 0,0,0,1 };
    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);

    vpl::math::CStaticVector<type, 3> v3 = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 3,1,0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}
TYPED_TEST(TransformationTest, Rotation)
{
    using type = typename TestFixture::type;

    TestFixture::transformMatrix.makeRotate(TestFixture::quat);

    type testTm[] = { 0,-1,0,0,1,0,0,0,0,0,1,0,0,0,0,1 };
    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);

    vpl::math::CStaticVector<type, 3> v3 = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 0,1,0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}

TYPED_TEST(TransformationTest, Scale)
{
    using type = typename TestFixture::type;

    TestFixture::transformMatrix.makeScale(3.0, 2.0, 0.0);

    type testTm[] = { 3,0,0,0, 0,2,0,0, 0,0,0,0, 0,0,0,1 };
    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);

    vpl::math::CStaticVector<type, 3> v3 = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 3,0,0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}

TYPED_TEST(TransformationTest, RotationAndTranslation)
{
    using type = typename TestFixture::type;

    TestFixture::transformMatrix = vpl::math::CTransformMatrix<type>(TestFixture::quat);
    TestFixture::transformMatrix =
        vpl::math::CTransformMatrix<type>
        ::translate(vpl::math::CStaticVector<type, 3>(2.0, 1.0, 0.0)) * TestFixture::transformMatrix;

    type testTm[] = { 0,-1,0,2, 1,0,0,1, 0,0,1,0, 0,0,0,1 };
    TestFixture::compare2D.values(testTm, TestFixture::transformMatrix, 4, 4);

    vpl::math::CStaticVector<type, 3> v3 = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 2,2,0 };
    TestFixture::compareStaticVector.values(test, v3, 3);
}

//! Pre mult tests.
TYPED_TEST(TransformationTest, PreMult)
{
    using type = typename TestFixture::type;

    TestFixture::transformMatrix.unit();

    TestFixture::transformMatrix.preMultTranslate(vpl::math::CDVector3(2.0, 1.0, 3.0));
    vpl::math::CStaticVector<type, 3> vector = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 3,1,3 };
    TestFixture::compareStaticVector.values(test, vector, 3);

    TestFixture::transformMatrix.preMultRotate(TestFixture::quat);
    vector = TestFixture::transformMatrix * TestFixture::vector3A;
    type test2[] = { -1,3,3 };
    TestFixture::compareStaticVector.values(test2, vector, 3);



    TestFixture::transformMatrix.preMultScale(vpl::math::CDVector3(2.0, -1.0, 3.0));
    vector = TestFixture::transformMatrix * TestFixture::vector3A;
    type test3[] = { -2,-3,9 };
    TestFixture::compareStaticVector.values(test3, vector, 3);
}

//! Post mult tests.
TYPED_TEST(TransformationTest, PostMult)
{
    using type = typename TestFixture::type;

    TestFixture::transformMatrix.unit();

    TestFixture::transformMatrix.postMultTranslate(vpl::math::CDVector3(2.0, 1.0, 3.0));
    vpl::math::CStaticVector<type, 3> vector = TestFixture::transformMatrix * TestFixture::vector3A;
    type test[] = { 3,1,3 };
    TestFixture::compareStaticVector.values(test, vector, 3);

    TestFixture::transformMatrix.postMultRotate(TestFixture::quat);
    vector = TestFixture::transformMatrix * TestFixture::vector3A;
    type test2[] = { 2,2,3 };
    TestFixture::compareStaticVector.values(test2, vector, 3);

    TestFixture::transformMatrix.postMultScale(vpl::math::CDVector3(2.0, -1.0, 3.0));
    vector = TestFixture::transformMatrix * TestFixture::vector3A;
    type test3[] = { 2,3,3 };
    TestFixture::compareStaticVector.values(test3, vector, 3);
}

TEST(TransformationTest, Decomposition)
{
    vpl::math::CTransformMatrix<double> transformMatrix;
    const vpl::math::CDQuat quat(vpl::math::HALF_PI,
        vpl::math::CDVector3(0.0, 0.0, 1.0));


    transformMatrix.unit();
    transformMatrix.postMultTranslate(vpl::math::CDVector3(2.0, 1.0, 3.0));
    transformMatrix.postMultRotate(quat);
    transformMatrix.postMultScale(vpl::math::CDVector3(1.0, 2.0, 3.0));
    
    vpl::math::CDTransformMatrix trans, rot, scale;
    transformMatrix.decompose(trans, rot, scale);

    vpl::test::Compare1D<double, vpl::math::CStaticVector<double, 3>> compareStaticVector;
    vpl::test::Compare1D<double, vpl::math::CQuat<double>> compareQuat;
    vpl::test::Compare2D<double, vpl::math::CTransformMatrix<double>> compare2D;

    compareStaticVector.setErrorMessage("Vectors is differ at index");
    compareQuat.setErrorMessage("Quat is different at index");

    compare2D.setArrayOrder(vpl::test::ArrayOrder::COLUMN_MAJOR);


    double translateReq[] = { 1,0,0,2, 0,1,0,1, 0,0,1,3, 0,0,0,1 };
    compare2D.values(translateReq, trans, 4, 4);

    double rotationReq[] = { 0,-1,0,0, 1,0,0,0, 0,0,1,0, 0,0,0,1 };
    compare2D.values(rotationReq, rot, 4, 4);

    double scaleReq[] = { 1,0,0,0, 0,2,0,0, 0,0,3,0, 0,0,0,1 };
    compare2D.values(scaleReq, scale, 4, 4);


    vpl::math::CDVector3 tv, sv;
    vpl::math::CDQuat rq;
    transformMatrix.decompose(tv, rq, sv);

    double transVecReq[] = { 2,1,3 };
    compareStaticVector.values(transVecReq, tv, 3);

    double rotVecReq[] = { 0,0,0.707,0.707 };
    compareQuat.values(rotVecReq, rq, 4,0.01);

    double scaleVecReq[] = { 1,2,3 };
    compareStaticVector.values(scaleVecReq, sv, 3);
}
}
#endif