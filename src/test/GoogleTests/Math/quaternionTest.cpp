//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Math/Quaternion.h>
#include <VPL/Math/StaticVector.h>
#include <VPL/Math/TransformMatrix.h>

namespace quaternion
{
//! Test fixture
template<class T>
class QuaternionTest : public testing::Test
{
public:
    using type = T;
};

//! Define types for typed testing
typedef testing::Types<double, float> implementations;
TYPED_TEST_CASE(QuaternionTest, implementations);

//! Testing base initialization
TYPED_TEST(QuaternionTest, Initialize)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0(0, 0, 0, 0);
    EXPECT_EQ(0, q0.length());

    // Check that equivalent constructor calls create equivalent objects
    vpl::math::CQuat<type> q1(1, 0, 0, 0);
    vpl::math::CQuat<type> q2(vpl::math::CStaticVector<type, 3>{1, 0, 0});
    vpl::math::CQuat<type> q3(q1.asEigen());

    EXPECT_EQ(q1, q2);
    EXPECT_EQ(q2, q3);
}

//! Testing quaternion length
TYPED_TEST(QuaternionTest, Length)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0(1, 2, 2, 0);
    vpl::math::CQuat<type> q1(2, 10, 0, 11);

    EXPECT_EQ(q0.length(), 3);
    EXPECT_EQ(q1.length(), 15);

    EXPECT_EQ(q0.length2(), 9);
    EXPECT_EQ(q1.length2(), 225);
}

//! Testing quaternion length
TYPED_TEST(QuaternionTest, LengthAsEigen)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0(0, 0, -vpl::math::SQRT2/2, vpl::math::SQRT2/2);
    vpl::math::CQuat<type> q1(1, 2, 2, 0);
    vpl::math::CQuat<type> q2(2, 10, 0, 11);
    EXPECT_EQ(q0.length(), q0.asEigen().norm());
    EXPECT_EQ(q1.length(), q1.asEigen().norm());
    EXPECT_EQ(q2.length(), q2.asEigen().norm());
}

//! Testing the dot product
TYPED_TEST(QuaternionTest, Dot)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0{ 1, 2, 0.5, 0.2 };
    vpl::math::CQuat<type> q1{ 4, 2, 8, 20 };
    const auto dot = q0.dot(q1);
    EXPECT_EQ(dot, 16);
}

//! Testing "rotating" constructors
TYPED_TEST(QuaternionTest, InitializeByRotation)
{
    using type = typename TestFixture::type;

    // Check "rotating" constructors
    vpl::math::CStaticVector<type, 3> v1{0, 1, 0};
    vpl::math::CStaticVector<type, 3> v2{1, 0, 0};
    vpl::math::CStaticVector<type, 3> axis{0, 0, 1};
    type angle = -vpl::math::HALF_PI;

    vpl::math::CQuat<type> q1(v1, v2);
    vpl::math::CQuat<type> q2(angle, axis);
    vpl::math::CQuat<type> q3(0, 0, -vpl::math::SQRT2/2, vpl::math::SQRT2/2);

    // Using dot product to allow slight differences
    EXPECT_GT(q1.dot(q3), 0.999);
    EXPECT_GT(q2.dot(q3), 0.999);
    EXPECT_GT(q1.dot(q2), 0.999);
}

//! Testing accessors
TYPED_TEST(QuaternionTest, Access)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q(1, 2, 3, 4);
    EXPECT_EQ(1, q[0]);
    EXPECT_EQ(1, q(0));
    EXPECT_EQ(1, q.at(0));
    EXPECT_EQ(1, q.x());

    EXPECT_EQ(2, q[1]);
    EXPECT_EQ(3, q.at(2));

    EXPECT_EQ(4, q[3]);
    EXPECT_EQ(4, q(3));
    EXPECT_EQ(4, q.at(3));
    EXPECT_EQ(4, q.w());
}

//! Testing basic quaternion arithmetic
TYPED_TEST(QuaternionTest, Arithmetic)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0(1, 2, 3.5, 4);
    vpl::math::CQuat<type> q1(2, 4, 7, 8);

    EXPECT_EQ(q0*2, q1);
    EXPECT_EQ(q0.length()*2, q1.length());

    EXPECT_EQ(q1/2, q0);
    EXPECT_EQ(q1.length()/2, q0.length());
}

//! Testing quaternion multiplication
TYPED_TEST(QuaternionTest, Multiply)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0{ 3, 2, 3, 2 };
    vpl::math::CQuat<type> q1{ 2, 3, 2, 3 };
    vpl::math::CQuat<type> expected{ 8, 12, 18, -12 };
    const auto result = q0 * q1;
    EXPECT_EQ(result, expected);
}

//! Testing that quaternion multiplication works the same way as it does in Eigen
TYPED_TEST(QuaternionTest, MultiplyAsEigen)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0{ .2, .1, .5, 1. };
    vpl::math::CQuat<type> q1{ .3, .3, .1, .2 };
    const auto r0 = q0 * q1;
    const auto r1 = q0.asEigen() * q1.asEigen();
    EXPECT_EQ(r0, r1);
}

//! Testing quaternion conjugate (by comparison with Eigen)
TYPED_TEST(QuaternionTest, ConjugateAsEigen)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0(0, 0, -vpl::math::SQRT2/2, vpl::math::SQRT2/2);
    const auto result = q0.conjugate();
    const auto expected = q0.asEigen().conjugate();
    EXPECT_EQ(result.coeffs(), expected.coeffs());
}

//! Testing quaternion inversion
TYPED_TEST(QuaternionTest, InversionAsEigen)
{
    using type = typename TestFixture::type;

    vpl::math::CQuat<type> q0(0, 0, -vpl::math::SQRT2/2, vpl::math::SQRT2/2);
    const auto result = q0.inverse();
    const auto expected = q0.asEigen().inverse();
    EXPECT_EQ(result.coeffs(), expected.coeffs());
}

//! Testing vector rotation
TYPED_TEST(QuaternionTest, RotateVector)
{
    using type = typename TestFixture::type;

    // Values for defining quaternion rotations
    vpl::math::CStaticVector<type, 3> v1{0, 1, 0};
    vpl::math::CStaticVector<type, 3> v2{1, 0, 0};
    vpl::math::CStaticVector<type, 3> axis{1, 0, 0};
    type angle = vpl::math::HALF_PI;

    // Rotation quaternions
    vpl::math::CQuat<type> q1(v1, v2); // 90deg around -Z axis
    vpl::math::CQuat<type> q2(angle, axis); // 90deg around +X axis

    vpl::math::CStaticVector<type, 3> input(0, 1, 0);
    vpl::math::CStaticVector<type, 3> expected1(1, 0, 0);
    vpl::math::CStaticVector<type, 3> expected2(0, 0, 1);
    const auto r1 = q1 * input;
    const auto r2 = q2 * input;

    // Using dot product to allow slight differences
    EXPECT_GT(r1*expected1, 0.999);
    EXPECT_GT(r2*expected2, 0.999);
}

//! Testing CQuat::set() functions
TYPED_TEST(QuaternionTest, InitializationBySetFunc)
{
    using type = typename TestFixture::type;

    { // Initialize by elements
        vpl::math::CQuat<type> q;
        q.set(1, 1, 1, 1);
        EXPECT_EQ(2, q.length());
    }

    { // Initialize by angle-axis
        vpl::math::CQuat<type> q;

        vpl::math::CStaticVector<type, 3> axis{0, 0, 1};
        type angle = -vpl::math::HALF_PI;
        q.set(angle, axis);

        // Length shouldn't change (significantly)
        EXPECT_TRUE(q.length() > 0.9999 && q.length() < 1.0001);

        // Expected result
        vpl::math::CQuat<type> expected(0, 0, -vpl::math::SQRT2/2, vpl::math::SQRT2/2);
        EXPECT_GT(q.dot(expected), 0.999);
    }

    { // Initialize by matrix
        vpl::math::CQuat<type> q;

        vpl::math::CStaticMatrix<type, 3, 3> m3;
        vpl::math::CStaticMatrix<type, 4, 4> m4;
        vpl::math::CTransformMatrix<type> tm;
        m3.unit();
        m4.unit();
        tm.unit();

        q.set(vpl::math::CTransformMatrix<type>{ m3 });
        EXPECT_EQ(1, q.length());
        EXPECT_TRUE(q.zeroRotate());

        q.set(vpl::math::CTransformMatrix<type>{ m4 });
        EXPECT_EQ(1, q.length());
        EXPECT_TRUE(q.zeroRotate());

        q.set(tm);
        EXPECT_EQ(1, q.length());
        EXPECT_TRUE(q.zeroRotate());
    }
}

TYPED_TEST(QuaternionTest, MakeRotate)
{
    using type = typename TestFixture::type;

    // Values for defining quaternion rotations
    vpl::math::CStaticVector<type, 3> v1{0, 1, 0};
    vpl::math::CStaticVector<type, 3> v2{1, 0, 0};
    vpl::math::CStaticVector<type, 3> axis{0, 0, 1};
    type angle = -vpl::math::HALF_PI;

    // Rotation quaternions
    vpl::math::CQuat<type> q1, q2, q3, q4;

    q1.makeRotate(angle, axis);
    q2.makeRotate(angle, 0, 0, 1);
    q3.makeRotate(v1, v2);
    q4.set(angle, axis);

    // Using dot product to allow slight differences
    EXPECT_GT(q1.dot(q2), 0.999);
    EXPECT_GT(q2.dot(q3), 0.999);
    EXPECT_GT(q1.dot(q3), 0.999);
    EXPECT_GT(q1.dot(q4), 0.999);
}

}
