//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"

#include <VPL/System/ScopedLock.h>
#include <VPL/System/Sleep.h>
#include <VPL/System/Thread.h>

namespace thread
{
//==============================================================================
//! Global constants.
namespace settings
{
const int numberOfThreads = 5;
const int delay = 50;
}

//! Smart pointer to critical section
vpl::sys::CMutex        Mutex;



int buffer[settings::numberOfThreads];

//==============================================================================
//! Thread routine

VPL_THREAD_ROUTINE(thread)
{
    const int id = *(reinterpret_cast<int *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        {
            vpl::sys::tScopedLock Guard(Mutex);
            buffer[id]++;
        }
    vpl::sys::sleep(settings::delay + settings::delay * id);
    }

    vpl::sys::tScopedLock Guard(Mutex);
    buffer[id] += 100;
    return 0;
}

//! Create threads and checks valid values in buffer.
TEST(ThreadTest, Base)
{
    vpl::sys::CThread *pThreads[settings::numberOfThreads];
    int piThreadsId[settings::numberOfThreads];

    // Creation of all ppThreads
    for (int i = 0; i < settings::numberOfThreads; i++)
    {
        piThreadsId[i] = i;
        pThreads[i] = new vpl::sys::CThread(thread, static_cast<void *>(&piThreadsId[i]), true);
    }

    vpl::sys::sleep(1000);


    for (int i = 0; i < settings::numberOfThreads; i++)
    {
        pThreads[i]->terminate(true, 1000);
        delete pThreads[i];
    }

	// Check test buffer
    for (int i = 0; i < settings::numberOfThreads; i++)
    {
        ASSERT_LE(101, buffer[i]);
    }
}
}