#################################################################################
# This file is part of
#
# VPL - Voxel Processing Library
# Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#################################################################################

#Add sources directory - adds all source files from the directory
macro( ADD_SOURCE_DIRECTORY _DIR )
    file( GLOB_RECURSE _VPL_SOURCES ${_DIR}/*.c ${_DIR}/*.cpp ${_DIR}/*.cc )
    list( APPEND VPL_SOURCES ${_VPL_SOURCES} )
endmacro( ADD_SOURCE_DIRECTORY )
                                                                                
# Add include directory - adds all headers from the directory
macro( ADD_HEADER_DIRECTORY _DIR )
    file( GLOB_RECURSE _VPL_HEADERS ${_DIR}/*.h ${_DIR}/*.hxx ${_DIR}/*.hpp )
list(LENGTH _VPL_HEADERS LENGTH)
    if( LENGTH GREATER 0 )
        list( APPEND VPL_HEADERS ${_VPL_HEADERS} )
        include_directories( ${_DIR} )
    endif( LENGTH GREATER 0 )
endmacro( ADD_HEADER_DIRECTORY )