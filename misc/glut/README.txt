Pre-compiled GLUT library for MinGW compiler and MS Visual C++.

1) MinGW - Simply unzip the 'mingw_glut-v3.7.6.zip' archive to the MinGW
   directory and place the DLL library 'glut32.dll' to your
   'Windows\system32' directory.

2) MS Visual C++ - Unzip the 'misc/vc_glut-v3.7.6.zip' file into your MSVC
   installation directory. The right path usually look like:
   C:\Program Files\Microsoft Visual Studio .NET 2003\Vc7\PlatformSDK
   Place the 'glut32.dll' file to your 'Windows\system32' directory.
